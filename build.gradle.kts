group = "dev.tasso"
version = "0.1"

plugins {
    kotlin("jvm") version "1.3.50"
    id("java-gradle-plugin")
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib", "1.3.50"))
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.4.0")
}

tasks.getByName<Wrapper>("wrapper") {
    gradleVersion = "5.6.2"
    distributionType = Wrapper.DistributionType.ALL
}

gradlePlugin {
    plugins {
        create("gradle-git") {
            id = "dev.tasso.gradle-git"
            implementationClass = "dev.tasso.gradlegit.GradleGitPlugin"
        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
