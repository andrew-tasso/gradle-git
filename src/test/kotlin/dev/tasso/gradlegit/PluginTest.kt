package dev.tasso.gradlegit

import io.kotlintest.shouldNotBe
import io.kotlintest.specs.WordSpec
import org.gradle.testfixtures.ProjectBuilder

class PluginTest : WordSpec({

    "Applying the plugin using the plugin ID" should {
        "Apply the plugin to the project" {
            val project = ProjectBuilder.builder().build()
            project.pluginManager.apply("dev.tasso.gradle-git")

            project.plugins.getPlugin(GradleGitPlugin::class.java) shouldNotBe null
        }
    }

})